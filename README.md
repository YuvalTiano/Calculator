# Calculator

##Live Example

Watch live example here:

[yuvaltiano.gitlab.io/Calculator/](https://yuvaltiano.gitlab.io/Calculator/ "Calculator Live Example")

##Introduction

This is a simple web app calculator that built with ReactJS for educational purpose.

Some of the highlight feature include:

- Using Local Storage to save the last session.
- Handling keyboard inputs.
- Mobile design.

![](https://i.imgur.com/fjS3UCK.gif)

Feel free to fork/modified the code.