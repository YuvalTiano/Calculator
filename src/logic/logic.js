import validOperators from "./validOperators";

export function handleKeyboardEvent({key}) {
    const keyOperator = {
        "13":"=", // Enter key for Chrome
        "0":"=", // Enter key for FireFox
        "46":".",
        "43":"+",
        "45":"-",
        "42":"x",
        "47":"/",
    };

    if (Object.keys(keyOperator).includes(key)) {
        return (keyOperator[key]);
    }

    const number = parseInt(key, 10);

    // Validate the number:
    if (0 <= number && number <= 9) {
        return number;
    }
}

export function logic(state, button) {
    if (state.screen === "ERROR") {
        if (button === "AC") {
            return logic({}, button)
        }

        return {
            screen: "ERROR"
        };
    }

    if (typeof button === "number") {
        button = button.toString();
        if (!state.screen || state.screen === 0) {
            if (button !== "0" || (button === "0" && !state.screen)) {
                return {
                    screen: button,
                };
            }
            else {
                return null;
            }
        }
        else {
            return {screen: state.screen.toString() + button}
        }
    }
    else if (button === ".") {
        if (!state.screen || state.screen === 0 || state.screen.includes(".")) {
            return null;
        }
        else {
            return {
                screen: `${state.screen.toString()}.`,
            }
        }
    }
    else if (Object.keys(validOperators).includes(button)) {
        if (state.screen) {
            return {
                prevNumber: state.screen,
                screen: null,
                operator: button,
            }
        }
        else {
            return {
                operator: button,
            };
        }
    }
    else if (button === "AC") {
        return {
            prevNumber: null,
            screen: null,
            operator: null,
        };
    }
    else if (button === "Del") {
        if (state.screen) {
            return {
                screen: state.screen.slice(0, -1),
            };
        }

        return {
            screen: null,
        };
    }
    else if (button === "=") {
        const a = parseFloat(state.prevNumber);
        const b = parseFloat(state.screen);
        const operator = state.operator.toString();

        return {
            prevNumber: state.screen,
            screen: calculate(a, b, operator),
        };
    }
    else {
        return {
            screen: "ERROR",
        };
    }
}

export function calculate(a, b, operation) {
    switch (operation) {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "x":
            return a * b;
        case "/":
            return (
                b === 0
                    ? "ERROR" // Preventing division by 0
                    : a / b
            );
        default:
            return "ERROR";
    }
}
