import React from "react";
import PropTypes from 'prop-types';
import CurrentNumber from "./CurrentNumber";
import CurrentOperator from "./CurrentOperator";

class Screen extends React.PureComponent {
    static propTypes = {
        currentNumber: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        currentOperator: PropTypes.string,
    };

    render() {
        const {
            currentOperator,
            currentNumber,
        } = this.props;

        const currentNumberToShow = (
            currentNumber !== 0
                ? currentNumber.toString().slice(0, 8)
                : 0
        );

        return (
            <div className="Screen">
                <CurrentNumber show={ currentNumberToShow } />
                <CurrentOperator show={ currentOperator } />
            </div>
        );
    };
}

export default Screen;
