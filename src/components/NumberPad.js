import React from "react";
import PropTypes from 'prop-types';
import Button from "./Button";

class NumberPad extends React.PureComponent {
    render() {
        const numbers = [7,8,9,4,5,6,1,2,3,0,"."];
        const {
            clickHandler,
        } = this.props;

        return (
            <div className="NumberPad">
                {
                    numbers.map((number) => (
                        <Button
                            key={ number }
                            clickHandler={ clickHandler }
                            name={ number } />
                    ))
                }
            </div>
        );
    };
}

NumberPad.propTypes = {
    clickHandler: PropTypes.func.isRequired,
}

export default NumberPad;
