import React from "react";
import PropTypes from 'prop-types';

class CurrentNumber extends React.PureComponent {
    render() {
        const {
            show,
        } = this.props;

        return <p>{show}</p>;
    };
}

CurrentNumber.propTypes = {
    show: PropTypes.oneOfType([
        PropTypes.number, 
        PropTypes.string,
    ]).isRequired,
}

export default CurrentNumber;
