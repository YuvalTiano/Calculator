import React from "react";
import Screen from "./Screen";
import Pad from "./Pad";
import {
    logic,
    handleKeyboardEvent
} from "../logic/logic";

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prevNumber:null,
            screen:null,
            operator:null,
        };
    };

    sendToLogic = (buttonName) => {
        this.setState(logic(this.state, buttonName));
    };

    componentDidMount() {
        document.addEventListener("keypress",(event) => this.sendToLogic(handleKeyboardEvent(event)));
        const lastVisit = localStorage.getItem("lastVisit");

        if (lastVisit) {
            this.setState(JSON.parse(lastVisit));
        }
    };

    componentDidUpdate() {
        localStorage.setItem("lastVisit", JSON.stringify(this.state));
    };

    render() {
        const {
            screen,
            prevNumber,
            operator,
        } = this.state;

        return (
            <div className="Calculator">
                <Screen currentNumber={screen || prevNumber || 0} currentOperator={operator}/>
                <Pad clickHandler={this.sendToLogic}/>
            </div>
        );
    };
}

export default Calculator;
