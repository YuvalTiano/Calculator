import React from "react";
import PropTypes from 'prop-types';
import Button from "./Button";
import validOperators from "../logic/validOperators";

class OperatorPad extends React.PureComponent {
    static propTypes = {
        clickHandler: PropTypes.func.isRequired,
    }

    render() {
        const {
            clickHandler,
        } = this.props;

        return (
            <div className="OperatorPad">
                {
                    Object.keys(validOperators).map((operator) => (
                        <Button
                            key={ validOperators[operator] }
                            clickHandler={ clickHandler }
                            name={ operator } />
                    ))
                }
                <Button clickHandler={ clickHandler } name="=" />
            </div>
        );
    };
}

export default OperatorPad;
