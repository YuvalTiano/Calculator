import React from "react";
import PropTypes from 'prop-types';

class Button extends React.PureComponent {
    static propTypes = {
        clickHandler: PropTypes.func.isRequired,
    }

    handleClick = () => {
        window.navigator.vibrate(60);
        this.props.clickHandler(this.props.name);
    };

    render() {
        const {
            display,
            name,
         } = this.props;

         return (
            <button onClick={ this.handleClick }>
                {
                    display
                        ? display
                        : name
                }
            </button>
        );
    };
}

export default Button;
