import React from "react";
import PropTypes from 'prop-types';

class CurrentOperator extends React.PureComponent {
    render() {
        const {
            show,
        } = this.props;

        return <p>{show || "!"}</p>;
    };
}

CurrentOperator.propTypes = {
    show: PropTypes.oneOfType([
        PropTypes.string,
    ])
}

export default CurrentOperator;
