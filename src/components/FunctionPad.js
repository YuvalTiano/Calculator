import React from "react";
import PropTypes from 'prop-types';
import Button from "./Button";

class FunctionPad extends React.PureComponent {
    static propTypes = {
        clickHandler: PropTypes.func.isRequired,
    }

    render() {
        const {
            clickHandler,
        } = this.props;

        return(
            <div className="FunctionPad">
                <Button clickHandler={ clickHandler } name="Del" />
                <Button clickHandler={ clickHandler } name="AC" />
            </div>
        );
    };
}

export default FunctionPad;
