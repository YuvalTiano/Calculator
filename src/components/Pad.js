import React from "react";
import PropTypes from 'prop-types';
import OperatorPad from "./OperatorPad";
import FunctionPad from "./FunctionPad";
import NumberPad from "./NumberPad";

class Pad extends React.PureComponent {
    static propTypes = {
        clickHandler: PropTypes.func.isRequired,
    }

    render() {
        const {
            clickHandler,
        } = this.props;

        return (
            <div className="Pad">
                <OperatorPad clickHandler={ clickHandler }/>
                <FunctionPad clickHandler={ clickHandler }/>
                <NumberPad clickHandler={ clickHandler }/>
            </div>
        );
    };
}

export default Pad;
